// Copyright 2021 Iglou.eu.
// Written by Adrien Kara <adrien@iglou.eu>
// Use of this source code is governed by GPL-3.0-or-later
// license that can be found in the LICENSE file.

package model

import (
	"os"
	"path/filepath"
	"syscall"
	"time"

	"git.iglou.eu/Laboratory/goCloudyFile/internal/config"
)

const bucketItem = "item"

type Items []Item
type Item struct {
	Hash  string
	Name  string
	Path  string
	Size  int64
	IsDir bool

	UID        int
	GID        int
	CanBeRead  bool
	CanBeWrite bool

	Mode     os.FileMode
	ModTime  time.Time
	Children *Items
}

// ItemFromOs returns an Item from an os.FileInfo.
// Use errors.Is to check the error type and handle it.
func ItemFromOs(path string, child ...bool) (item Item, err error) {
	// Get the item info
	fi, err := os.Stat(path)
	if err != nil {
		return
	}

	// Set item info
	item.Name = fi.Name()
	item.Path = path
	item.Size = fi.Size()
	item.Mode = fi.Mode()
	item.ModTime = fi.ModTime()
	item.IsDir = fi.IsDir()

	// Set item UID and GID
	item.UID = int(fi.Sys().(*syscall.Stat_t).Uid)
	item.GID = int(fi.Sys().(*syscall.Stat_t).Gid)

	// Set read/write permissions
	item.CanBeRead = item.IsReadable()
	item.CanBeWrite = item.IsWritable()

	// Initialize the children pointer
	item.Children = &Items{}

	// If the item is a file or child is false, return
	if !fi.IsDir() || (len(child) == 0 || !child[0]) {
		return
	}

	// Get children
	de, err := os.ReadDir(path)
	if err != nil {
		return
	}

	for _, e := range de {
		c, err := ItemFromOs(filepath.Join(path, e.Name()))
		if err != nil {
			continue
		}

		*item.Children = append(*item.Children, c)
	}

	return
}

// IsEmpty returns true if the item is empty.
func (item *Item) IsEmpty() bool {
	return *item == Item{}
}

// IsReadable returns true if the item can be read.
func (item *Item) IsReadable() bool {
	if item.Mode&(1<<2) != 0 {
		return true
	}

	if item.GID == config.Data.OS.GID && item.Mode&(1<<5) != 0 {
		return true
	}

	if item.UID == config.Data.OS.UID && item.Mode&(1<<8) != 0 {
		return true
	}

	return false
}

// IsWritable returns true if the item can be write.
func (item *Item) IsWritable() bool {
	if item.Mode&(1<<1) != 0 {
		return true
	}

	if item.GID == config.Data.OS.GID && item.Mode&(1<<4) != 0 {
		return true
	}

	if item.UID == config.Data.OS.UID && item.Mode&(1<<7) != 0 {
		return true
	}

	return false
}
