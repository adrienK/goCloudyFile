// Copyright 2021 Iglou.eu.
// Written by Adrien Kara <adrien@iglou.eu>
// Use of this source code is governed by GPL-3.0-or-later
// license that can be found in the LICENSE file.

package model

import (
	"git.iglou.eu/Laboratory/goCloudyFile/internal/config"
	"git.iglou.eu/Laboratory/goCloudyFile/internal/utils"

	bolt "go.etcd.io/bbolt"
)

var db *bolt.DB

// NewDB creates and opens a BoltDB database at the given path
// If the file does not exist then it will be created automatically
func NewDB() (err error) {
	// Set path to db file
	path := config.Data.App.WorkDir + "/bolt.db"

	// Open the BoltDB database
	db, err = bolt.Open(path, 0600, bolt.DefaultOptions)
	if err != nil {
		return
	}

	// Creates new buckets if the bucket does not exist
	// buckets := []string{bucketUser, bucketShare, bucketLog, bucketItem}
	buckets := []string{bucketItem}
	for _, bucket := range buckets {
		if err = CreateBucket(bucket); err != nil {
			return
		}
	}

	return
}

// CloseDB closes the BoltDB database
func CloseDB() error {
	return db.Close()
}

// CreateBucket creates a new bucket
// if the bucket already exists do nothing
func CreateBucket(bucket string) error {
	return db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucket))
		return err
	})
}

// SetBucketValue sets a key/value pair in a bucket
// if the key already exists, the value is overwritten
// if the value is nil, the key is deleted
func SetBucketValue(bucket, key string, value interface{}) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}

		if value == nil {
			return b.Delete([]byte(key))
		}

		g, err := utils.ToBin(value)
		if err != nil {
			return err
		}

		return b.Put([]byte(key), g)
	})
}

// GetBucketValue gets a key/value pair in a bucket
// if the key does not exist, the value is set to nil
// takes bucket and key as strings, a value as pointer to interface{} and returns error
func GetBucketValue(bucket, key string, value interface{}) error {
	return db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}

		return utils.FromBin(b.Get([]byte(key)), value)
	})
}

// GetBucketKeys returns all keys in a bucket

// GetBucketValues returns all values in a bucket
func GetBucketValues(bucket string, values interface{}) error {
	return db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}

		buf := make([]interface{}, 0)
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			var value interface{}
			if err := utils.FromBin(v, &value); err != nil {
				return err
			}

			values = append(buf, value)
		}
		values = buf

		return nil
	})
}

// DelBugetKey deletes a key/value pair in a bucket
func DelBucketKey(bucket, key string) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}

		return b.Delete([]byte(key))
	})
}
