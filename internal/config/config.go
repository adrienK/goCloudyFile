// Copyright 2021 Iglou.eu.
// Written by Adrien Kara <adrien@iglou.eu>
// Use of this source code is governed by GPL-3.0-or-later
// license that can be found in the LICENSE file.

package config

import (
	"encoding/json"
	"flag"
	"os"
	"strconv"
)

type Config struct {
	// Server configuration
	Server Server `json:"server"`
	// App configuration
	App App `json:"app"`
	// UI configuration
	UI UI `json:"ui"`

	// OS store the os information
	OS OS `json:"-"`
	// The Config file path
	ConfigFile string `json:"-"`
}

type Cert struct {
	// For enabling automatic certificate, delivered by Let's Encrypt
	// Require the Config.Server.Domain to be set and valid
	Auto bool `json:"auto"`

	// Certificate file path are uncompatible with the auto option
	// The path to the certificate
	Crt string `json:"crt"`
	// The path to the certificate key
	Key string `json:"key"`
}

type Server struct {
	// The port of the server
	Port string `json:"port"`
	// The host of the server
	Host string `json:"host"`
	// A valid domain name for the server
	Domain string `json:"domain"`
	// The compression level of server responses
	Compression int `json:"compression"`
	// The maximum body size of a request
	MaxBodySize int `json:"max_body_size"`

	// The certificate for the server
	Cert Cert `json:"cert"`
}

type App struct {
	// The name of the application
	Name string `json:"name"`
	// The path to the data directory
	// This is where exposed files are stored
	DataDir string `json:"data_dir"`
	// The path to the working directory
	// This is where the application variables are stored like the database
	WorkDir string `json:"work_dir"`
	// The debuging mode
	Debug bool `json:"debug"`
}

type UI struct {
	// The application meta information
	Meta Meta `json:"meta"`
}

type Meta struct {
	// The author of the application
	Author string `json:"author"`
	// The description of the application
	Description string `json:"description"`
	// The title prefix of the application
	TitlePrefix string `json:"title_prefix"`
}

type OS struct {
	// PID of the process
	PID int
	// GID of app user
	GID int
	// UID of app user
	UID int
}

// CLI struct is to store the command line arguments values
type CLI struct {
	Port       string
	Host       string
	WorkDir    string
	DataDir    string
	ConfigFile string
	Debug      bool
}

// Data is the global configuration
var Data Config
var cli CLI

// Initialize the configuration
func Init() {
	// Save the app system information
	Data.OS.PID = os.Getpid()
	Data.OS.GID = os.Getegid()
	Data.OS.UID = os.Geteuid()

	// Get configuration from the command line
	flag.StringVar(&cli.Port, "port", "", "The port exposed by the server")
	flag.StringVar(&cli.Host, "host", "", "The host addr where the server will listen")
	flag.StringVar(&cli.WorkDir, "work", "", "The path to the working directory\nThis is where the application variables are stored like the database")
	flag.StringVar(&cli.DataDir, "data", "", "The path to the data directory\nThis is where exposed files are stored")
	flag.StringVar(&cli.ConfigFile, "conf", "", "Set the path to the configuration file")
	flag.BoolVar(&cli.Debug, "debug", false, "If set, the application will be in debug mode")
	flag.Parse()

	// Set the default configuration values
	Data.SetDefaults()

	// Set the config from the environment variables
	Data.SetFromEnv()

	// Set the config from the config file
	if cli.ConfigFile != "" {
		Data.ConfigFile = cli.ConfigFile
	}

	if Data.ConfigFile != "" {
		Data.SetFromFile()
	}

	// Override the configuration values with the command line flags
	Data.SetFromCLI()
}

// SetDefaults set the default configuration values
func (c *Config) SetDefaults() {
	c.ConfigFile = "/etc/gocf.conf"

	c.Server.Port = "2179"
	c.Server.Host = "127.0.0.1"
	c.Server.Compression = 1
	c.Server.MaxBodySize = 12582912

	c.App.Name = "GoCloudyFile"
	c.App.DataDir = "/var/lib/gocloudyfile/data"
	c.App.WorkDir = "/var/lib/gocloudyfile"

	c.UI.Meta.Author = "Iglou.eu"
	c.UI.Meta.Description = "GoCloudyFile is a web application for managing your content"
	c.UI.Meta.TitlePrefix = "MyCloudy - "
}

// SetFromEnv set the configuration values from the environment variables
func (c *Config) SetFromEnv() {
	if cfile := os.Getenv("GOCLOUDYFILE_CONFIG"); cfile != "" {
		c.ConfigFile = cfile
	}

	if port := os.Getenv("GOCLOUDYFILE_PORT"); port != "" {
		c.Server.Port = port
	}
	if host := os.Getenv("GOCLOUDYFILE_HOST"); host != "" {
		c.Server.Host = host
	}
	if dataDir := os.Getenv("GOCLOUDYFILE_DATA_DIR"); dataDir != "" {
		c.App.DataDir = dataDir
	}
	if workDir := os.Getenv("GOCLOUDYFILE_WORK_DIR"); workDir != "" {
		c.App.WorkDir = workDir
	}

	if debug, err := strconv.ParseBool(os.Getenv("GOCLOUDYFILE_DEBUG")); err == nil {
		c.App.Debug = debug
	}
}

// SetFromFile set the configuration values from a json file
// If the file does not exist, it will be created with the default values
func (c *Config) SetFromFile() error {
	// Check if the file exists with os.Open
	f, err := os.Open(c.ConfigFile)
	if os.IsNotExist(err) {
		// The file does not exist, create it
		return c.save()
	} else if err != nil {
		return err
	}
	defer f.Close()

	// Unmarshal the file into a new Config struct
	var newConfig Config
	if err := json.NewDecoder(f).Decode(&newConfig); err != nil {
		return err
	}

	// Merge the new config with the current one
	c.merge(&newConfig)

	return nil
}

// SeteFromCLI set the configuration values with the command line flags
func (c *Config) SetFromCLI() {
	if cli.Port != "" {
		c.Server.Port = cli.Port
	}
	if cli.Host != "" {
		c.Server.Host = cli.Host
	}
	if cli.WorkDir != "" {
		c.App.WorkDir = cli.WorkDir
	}
	if cli.DataDir != "" {
		c.App.DataDir = cli.DataDir
	}
	if cli.Debug {
		c.App.Debug = true
	}
}

// json.Marshal the configuration and save it to the file
func (c *Config) save() error {
	// Create the file
	f, err := os.Create(c.ConfigFile)
	if err != nil {
		return err
	}
	defer f.Close()

	// Marshal and save the configuration
	if err := json.NewEncoder(f).Encode(c); err != nil {
		return err
	}

	return nil
}

// merge the new config with the current one
// overwriting only the values that are not empty
func (c *Config) merge(newConfig *Config) {
	if newConfig.Server.Port != "" {
		c.Server.Port = newConfig.Server.Port
	}
	if newConfig.Server.Host != "" {
		c.Server.Host = newConfig.Server.Host
	}
	if newConfig.Server.Domain != "" {
		c.Server.Domain = newConfig.Server.Domain
	}
	if newConfig.Server.Compression != 0 {
		c.Server.Compression = newConfig.Server.Compression
	}
	if newConfig.Server.MaxBodySize > 1000000 {
		c.Server.MaxBodySize = newConfig.Server.MaxBodySize
	}
	if newConfig.Server.Cert.Crt != "" {
		c.Server.Cert.Crt = newConfig.Server.Cert.Crt
	}
	if newConfig.Server.Cert.Key != "" {
		c.Server.Cert.Key = newConfig.Server.Cert.Key
	}
	if !newConfig.Server.Cert.Auto {
		c.Server.Cert.Auto = newConfig.Server.Cert.Auto
	}
	if newConfig.App.Name != "" {
		c.App.Name = newConfig.App.Name
	}
	if newConfig.App.DataDir != "" {
		c.App.DataDir = newConfig.App.DataDir
	}
	if newConfig.App.WorkDir != "" {
		c.App.WorkDir = newConfig.App.WorkDir
	}
	if !newConfig.App.Debug {
		c.App.Debug = newConfig.App.Debug
	}
	if newConfig.UI.Meta.Author != "" {
		c.UI.Meta.Author = newConfig.UI.Meta.Author
	}
	if newConfig.UI.Meta.Description != "" {
		c.UI.Meta.Description = newConfig.UI.Meta.Description
	}
	if newConfig.UI.Meta.TitlePrefix != "" {
		c.UI.Meta.TitlePrefix = newConfig.UI.Meta.TitlePrefix
	}
}

// Addr returns the address of the server
func (s *Server) Addr() string {
	return s.Host + ":" + s.Port
}
