package utils

import (
	"encoding/hex"
	"strconv"

	kelbin "github.com/kelindar/binary"
)

// Htoa convert a hex slice to a string
func Htoa(h []byte) string {
	return hex.EncodeToString(h[:])
}

// ToBin encodes into binary format
// if the struct is nil, the gob is nil and the error also
func ToBin(v interface{}) (g []byte, err error) {
	if v == nil {
		return
	}

	return kelbin.Marshal(v)
}

// FromBin decodes from the binary format
// if the gob is nil, the struct is nil and the error also
func FromBin(g []byte, v interface{}) error {
	if g == nil {
		return nil
	}

	return kelbin.Unmarshal(g, v)
}

// HumanReadableSize return sizes in human readable format (e.g., 1K 234M 2G)
// If the size is negative, it returns an empty string.
// Take an int64 return a string
func HumanReadableSize(size int64) string {
	if size < 0 {
		return ""
	}

	units := []string{"O", "Ko", "Mo", "Go", "To", "Po"}

	i := 0
	f := float64(size)
	for i = range units {
		if f < 1024 {
			break
		}

		f /= 1024
	}

	return strconv.FormatFloat(f, 'f', 1, 64) + units[i]
}
