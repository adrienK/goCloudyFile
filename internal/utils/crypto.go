package utils

import "crypto/sha256"

// Hash returns the SHA-256 hash of a string
func Hash(s string) string {
	buf := sha256.Sum256([]byte(s))
	return Htoa(buf[:])
}
