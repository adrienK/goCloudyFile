package utils

import "strings"

// PathIsValid checks if the given path is valid.
// It checks for the Linux max length for NAME_MAX < 255 bytes and PATH_MAX < 4096.
// It must not contain "..", "//" or a nil byte and not start with a ".".
func PathIsValid(path string) bool {
	if len(path) > 4096 {
		return false
	}

	for _, f := range strings.Split(path, "/") {
		if len(f) > 255 {
			return false
		}
	}

	if strings.HasPrefix(path, ".") ||
		strings.Contains(path, "..") ||
		strings.Contains(path, "//") ||
		strings.ContainsRune(path, 0) {
		return false
	}

	return true
}
