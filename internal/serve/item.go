package serve

import (
	"bytes"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"git.iglou.eu/Laboratory/goCloudyFile/internal/config"
	"git.iglou.eu/Laboratory/goCloudyFile/internal/model"
	"git.iglou.eu/Laboratory/goCloudyFile/internal/utils"

	"github.com/gofiber/fiber/v2"
)

// itemOpen try to "opens" an item, can be a file or a folder
// If it is a folder, it will return the indexing page
// If it is a file, it will return the file or force download
func itemOpen(c *fiber.Ctx) error {
	// It is a download request
	var toDownload bool
	if c.Params("todl") == "true" {
		toDownload = true
	}

	// Get item uri and path
	itemOrigin := itemGetUri(c)
	itemPath := filepath.Join(config.Data.App.DataDir, itemOrigin)

	// Check if item path contains invalid characters
	if !utils.PathIsValid(itemPath) {
		return c.Status(400).SendString("Path contains invalid characters")
	}

	// Get item information
	item, err := model.ItemFromOs(itemPath, true)
	if err != nil {
		return itemError(c, err)
	}

	// If iten is a file
	if !item.IsDir {
		if toDownload {
			c.Set("Content-Disposition", "attachment; filename="+item.Name)
			c.Set("Content-Length", strconv.FormatInt(item.Size, 10))
		}

		return c.SendFile(itemPath)
	}

	// If item is a directory send indexing page
	index := bytes.NewBufferString("")
	if toDownload {
		index.WriteString("Cannot download a directory<br>")
	}
	for _, i := range *item.Children {
		if i.CanBeRead {
			index.WriteString(`- <a href="/item/` + filepath.Join(itemOrigin, i.Name) + `">` + i.Name + `</a>`)
		} else {
			index.WriteString(`- ` + i.Name)
		}
		if !i.IsDir {
			index.WriteString(` <a href="/item/dl.true/` + filepath.Join(itemOrigin, i.Name) + `">[Download]</a>`)
		}
		index.WriteString(` Size:` + utils.HumanReadableSize(i.Size))
		index.WriteString(` Modif:` + i.ModTime.Format(time.UnixDate))
		if i.IsDir {
			index.WriteString(` Nb:` + strconv.Itoa(len(*i.Children)))
		}
		index.WriteString(`<br>`)
	}

	c.Context().SetContentType("text/html")
	return c.Status(200).Send(index.Bytes())
}

// itemGetUri return the path given by the client
// Try to unescape the path and return it
// If it fails, return the path as is
func itemGetUri(c *fiber.Ctx) string {
	path := c.Params("*")

	// QueryUnescape only returns an error if the string contains any % not
	// followed by two hexadecimal digits, so we can safely ignore the error
	if qe, err := url.QueryUnescape(path); err == nil {
		return qe
	}

	return path
}

// itemError return the error type to the client
func itemError(c *fiber.Ctx, err error) error {
	switch {
	case os.IsNotExist(err):
		return c.Status(404).SendString("File not found")
	case os.IsPermission(err):
		return c.Status(403).SendString("Permission denied")
	}

	return c.Status(500).SendString("Internal server error")
}
