// Copyright 2021 Iglou.eu.
// Written by Adrien Kara <adrien@iglou.eu>
// Use of this source code is governed by GPL-3.0-or-later
// license that can be found in the LICENSE file.

package serve

import (
	"git.iglou.eu/Laboratory/goCloudyFile/internal/config"

	"github.com/gofiber/fiber/v2"
)

func Start() error {
	app := fiber.New(fiber.Config{
		CaseSensitive:        true,
		StrictRouting:        true,
		CompressedFileSuffix: ".gcf.gz",

		AppName:               config.Data.App.Name,
		BodyLimit:             config.Data.Server.MaxBodySize,
		ServerHeader:          config.Data.UI.Meta.Description,
		DisableStartupMessage: !config.Data.App.Debug,
	})

	// sign := app.Group("/sign")
	// sign.Get("/out", signOut())
	// sign.Post("/in", sessInit(), signIn())

	item := app.Group("/item")
	item.Get("/dl.:todl/*", itemOpen)
	item.Get("/*", itemOpen)

	return app.Listen(config.Data.Server.Addr())
}
