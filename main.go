// Copyright 2021 Iglou.eu.
// Written by Adrien Kara <adrien@iglou.eu>
// Use of this source code is governed by GPL-3.0-or-later
// license that can be found in the LICENSE file.

package main

import (
	"log"

	"git.iglou.eu/Laboratory/goCloudyFile/internal/config"
	"git.iglou.eu/Laboratory/goCloudyFile/internal/model"
	"git.iglou.eu/Laboratory/goCloudyFile/internal/serve"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func main() {
	// Load configuration
	config.Init()

	// Load database
	model.NewDB()

	// Start the server
	serve.Start()
}
