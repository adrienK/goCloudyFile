# [WIP] goCloudyFile

[![Go Report Card](https://goreportcard.com/badge/git.iglou.eu/Laboratory/goCloudyFile?style=flat-square)](https://goreportcard.com/report/git.iglou.eu/Laboratory/goCloudyFile)
[![License: GPL 3.0](https://img.shields.io/badge/License-GPL_3.0_or_later-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.html)

## Description
GoCloudyFile is a free software project under development,     
which aims to provide a simple and easy to use online file manager.    
Designed to be Self-Hosted, efficient, optimized, eco-designed, sober and secure.    

For the benefit of individuals, associations and small businesses.    
He is perfect in replacement of heavy and consuming project like OwnCloud, especially when you don't need all of their useless features.    

## Eco-Design rules
- Simple HTML and CSS in front-end
- Flat file database (no db like MySQL, MongoDB, PostgreSQL, etc.)
- No server side scripting (no NodeJS, no PHP, no Python, no Ruby, no Perl, etc.)
- LOW client side scripting (no AngularJS, no ReactJS, no VueJS, no WebAssembly, etc.)
- No external services required or used (except for Let's Encrypt certificate)
- No complex operations, listings, sync

## Functions
### File manager
- [ ] Add (Drag & Drop, Selection, Folder)
- [x] Open
- [ ] Remove
- [x] Explore
- [x] Download
- [ ] Touch/Mkdir
- [ ] Move file and folder

### Collaboration
- [ ] File Sharin (Expiration, Password)
- [ ] User Management (DualAuth, Read/Write restriction, Disable)
- [ ] Access/Usage Log

### Server
- [ ] Port redirection
- [ ] Manual cert
- [ ] Auto cert with Let’s Encrypt
- [ ] Compression level
- [x] "Database" in flat file

## Configuration

### Definitions
**Configuration file `cli:"conf"` or `env:"GOCLOUDYFILE_CONFIG"`**    
Determines the path of the config file.    
>Default: `/etc/gocf.conf`

#### # Server
**Port `cli:"port"` or `env:"GOCLOUDYFILE_PORT"` or `json:"port"`**    
The port exposed by the server.    
>Default: `2179`

**Host `cli:"host"` or `env:"GOCLOUDYFILE_HOST"` or `json:"host"`**    
The host addr where the server will listen.    
>Default: `127.0.0.1`

**Domain `json:"domain"`**    
A valid domain name, where the server will be accessible from internet.    
It used for the auto certification by Let’s Encrypt.    
>Default: `<nil>`

**Compression `json:"compression"`**    
Determines the compression algoritm user for the server responses.    
>Default: `LevelDefault    `
>>LevelDisabled:         -1    
LevelDefault:          0    
LevelBestSpeed:        1    
LevelBestCompression:  2    

**Max body size`json:"max_body_size"`**    
The maximum size of a request body.    
Given in bytes.    
>Default: `12582912`

#### # Cert
**Auto cert `json:"auto"`**    
If true, the server will be automatically certified by Let’s Encrypt.    
Require te Domain name to be set and valid.    
>Default: `false`

**Cert file `json:"crt"`**  
The path to the certificate file.    
This option is ignored if the auto cert option is set to true.  
>Default: `<nil>`

**Key file `json:"key"`**    
The path to the certificate key file.    
This option is ignored if the auto cert option is set to true.  
>Default: `<nil>`

#### # App
**Name `json:"name"`**    
The name of the application.    
Is used to define the title of the application.    
>Default: `GoCloudyFile`

**Data dir `cli:"data"` or `env:"GOCLOUDYFILE_DATA_DIR"` or `json:"data_dir"`**    
The path to the data directory.    
This is where exposed files are stored.    
>Default: `/var/lib/gocloudyfile/data`

**Work dir `cli:"work"` or `env:"GOCLOUDYFILE_WORK_DIR"` or `json:"work_dir"`**    
The path to the working directory.    
This is where the application variables are stored like the database.    
>Default: `/var/lib/gocloudyfile`

**Debug `cli:"debug"` or `env:"GOCLOUDYFILE_DEBUG"` or `json:"debug"`**    
If true, the application will be in debug mode.    
>Default: `false`

#### # Meta
**Author `json:"author"`**    
The name of the company or person who created the application or hosting the service.    
>Default: `Iglou.eu`

**Description `json:"description"`**  
A short description of the application or hosting service.    
It is used in the meta tags of the application and in the header of the application.  
>Default: `GoCloudyFile is a web application for managing your content`

**Title prefix `json:"title_prefix"`**    
The prefix of the page title in the navigation bar.    
>Default: `MyCloudy - `

### By CLI
_override all other defined configuration values_

```man
Usage of :
  -conf string
     Set the path to the configuration file
  -data string
     The path to the data directory
     This is where exposed files are stored
  -debug
     If set, the application will be in debug mode
  -host string
     The host addr where the server will listen
  -port string
     The port exposed by the server
  -work string
     The path to the working directory
     This is where the application variables are stored like the database
```

### By Environment variable 
_Overrides by the json config file_

>`GOCLOUDYFILE_PORT="2179"`    
`GOCLOUDYFILE_HOST="127.0.0.1"`    
`GOCLOUDYFILE_DATA_DIR="/tmp/data"`    
`GOCLOUDYFILE_WORK_DIR="/tmp"`    
`GOCLOUDYFILE_CONFIG="/tmp/config.json"`    
`GOCLOUDYFILE_DEBUG="true"`    

### By JSON
```json
{
  "server": {
    "port": "2179",
    "host": "127.0.0.1",
    "domain": "",
    "compression": 1,
    "max_body_size": 12582912,
    "cert": {
      "crt": "",
      "key": "",
      "auto": false
    }
  },
  "app": {
    "name": "GoCloudyFile",
    "debug": true,
    "data_dir": "/tmp/data",
    "work_dir": "/tmp"
  },
  "ui": {
    "meta": {
      "author": "GoCloudyFile",
      "description": "GoCloudyFile is a web application for managing your content",
      "title_prefix": "MyCloudy - "
    }
  }
}
```
