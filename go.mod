module git.iglou.eu/Laboratory/goCloudyFile

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.21.0
	github.com/kelindar/binary v1.0.14
	go.etcd.io/bbolt v1.3.6
)
