// Bench from https://github.com/hermanschaaf/go-string-concat-benchmarks
// BenchmarkByteSliceSize10-4           5918437           177.0 ns/op       176 B/op          2 allocs/op
// BenchmarkBufferSize10-4              5517441           199.5 ns/op       176 B/op          2 allocs/op
// BenchmarkBufferString10-4            5397687           209.2 ns/op       144 B/op          2 allocs/op
// BenchmarkJoinSize10-4                3766814           309.6 ns/op       224 B/op          2 allocs/op

// BenchmarkByteSliceSize100-4           820064          1444 ns/op        1536 B/op          2 allocs/op
// BenchmarkBufferSize100-4              718032          1666 ns/op        1536 B/op          2 allocs/op
// BenchmarkJoinSize100-4                506410          2363 ns/op        2304 B/op          2 allocs/op

// BenchmarkByteSliceSize1000-4           62649         17201 ns/op       15616 B/op          2 allocs/op
// BenchmarkBufferSize1000-4              56713         18815 ns/op       15616 B/op          2 allocs/op
// BenchmarkJoinSize1000-4                47352         24449 ns/op       21760 B/op          2 allocs/op

// BenchmarkByteSliceSize10000-4           3176        317639 ns/op      163840 B/op          2 allocs/op
// BenchmarkBufferSize10000-4              3709        326880 ns/op      163840 B/op          2 allocs/op
// BenchmarkJoinSize10000-4                3018        389122 ns/op      221184 B/op          2 allocs/op
package benchmark

import (
	"bytes"
	"strconv"
	"strings"
	"testing"
)

var junk map[int]string

func init() {
	junk = make(map[int]string)
	for i := 0; i < 10000; i++ {
		junk[i] = strconv.Itoa(i + 10000)
	}
}

// nextString is an iterator we use to represent a process
// that returns strings that we want to concatenate in order.
func nextString() func() string {
	n := 0
	// closure captures variable n
	return func() string {
		n += 1
		return junk[n]
	}
}

var global string

// benchmarkNaiveConcat provides a benchmark for basic built-in
// Go string concatenation. Because strings are immutable in Go,
// it performs the worst of the tested methods. The time taken to
// set up the array that is appended is not counted towards the
// time for naive concatenation.
func benchmarkNaiveConcat(b *testing.B, numConcat int) {
	// Reports memory allocations
	b.ReportAllocs()

	var ns string
	for i := 0; i < b.N; i++ {
		next := nextString()
		ns = ""
		for u := 0; u < numConcat; u++ {
			ns += next()
		}
	}
	// we assign to a global variable to make sure compiler
	// or runtime optimizations don't skip over the operations
	// we were benchmarking. This might be unnecessary, but it's
	// safe.
	global = ns
}

func BenchmarkNaiveConcat10(b *testing.B) {
	benchmarkNaiveConcat(b, 10)
}

func BenchmarkNaiveConcat100(b *testing.B) {
	benchmarkNaiveConcat(b, 100)
}

func BenchmarkNaiveConcat1000(b *testing.B) {
	benchmarkNaiveConcat(b, 1000)
}

func BenchmarkNaiveConcat10000(b *testing.B) {
	benchmarkNaiveConcat(b, 10000)
}

// benchmarkByteSlice provides a benchmark for the time it takes
// to repeatedly append returned strings to a byte slice, and
// finally casting the byte slice to string type.
func benchmarkByteSlice(b *testing.B, numConcat int) {
	// Reports memory allocations
	b.ReportAllocs()

	var ns string
	for i := 0; i < b.N; i++ {
		next := nextString()
		b := []byte{}
		for u := 0; u < numConcat; u++ {
			b = append(b, next()...)
		}
		ns = string(b)
	}
	global = ns
}

func BenchmarkByteSlice10(b *testing.B) {
	benchmarkByteSlice(b, 10)
}

func BenchmarkByteSlice100(b *testing.B) {
	benchmarkByteSlice(b, 100)
}

func BenchmarkByteSlice1000(b *testing.B) {
	benchmarkByteSlice(b, 1000)
}

func BenchmarkByteSlice10000(b *testing.B) {
	benchmarkByteSlice(b, 10000)
}

// benchmarkByteSlice provides a benchmark for the time it takes
// to repeatedly append returned strings to a byte slice, and
// finally casting the byte slice to string type.
func benchmarkByteSliceSize(b *testing.B, numConcat int) {
	// Reports memory allocations
	b.ReportAllocs()

	var ns string
	for i := 0; i < b.N; i++ {
		next := nextString()
		b := make([]byte, 0, numConcat*10)
		for u := 0; u < numConcat; u++ {
			b = append(b, next()...)
		}
		ns = string(b)
	}
	global = ns
}

func BenchmarkByteSliceSize10(b *testing.B) {
	benchmarkByteSliceSize(b, 10)
}

func BenchmarkByteSliceSize100(b *testing.B) {
	benchmarkByteSliceSize(b, 100)
}

func BenchmarkByteSliceSize1000(b *testing.B) {
	benchmarkByteSliceSize(b, 1000)
}

func BenchmarkByteSliceSize10000(b *testing.B) {
	benchmarkByteSliceSize(b, 10000)
}

// benchmarkJoin provides a benchmark for the time it takes to set
// up an array with strings, and calling strings.Join on that array
// to get a fully concatenated string.
func benchmarkJoin(b *testing.B, numConcat int) {
	// Reports memory allocations
	b.ReportAllocs()

	var ns string
	for i := 0; i < b.N; i++ {
		next := nextString()
		a := []string{}
		for u := 0; u < numConcat; u++ {
			a = append(a, next())
		}
		ns = strings.Join(a, "")
	}
	global = ns
}

func BenchmarkJoin10(b *testing.B) {
	benchmarkJoin(b, 10)
}

func BenchmarkJoin100(b *testing.B) {
	benchmarkJoin(b, 100)
}

func BenchmarkJoin1000(b *testing.B) {
	benchmarkJoin(b, 1000)
}

func BenchmarkJoin10000(b *testing.B) {
	benchmarkJoin(b, 10000)
}

// benchmarkJoinSize provides a benchmark for the time it takes to set
// up an array with strings, and calling strings.Join on that array
// to get a fully concatenated string – when the (approximate) number of
// strings is known in advance.
//
// This is identical to benchmarkJoin, except numConcat is used to size
// the []string slice's initial capacity to avoid needless reallocation.
func benchmarkJoinSize(b *testing.B, numConcat int) {
	// Reports memory allocations
	b.ReportAllocs()

	var ns string
	for i := 0; i < b.N; i++ {
		next := nextString()
		a := make([]string, 0, numConcat)
		for u := 0; u < numConcat; u++ {
			a = append(a, next())
		}
		ns = strings.Join(a, "")
	}
	global = ns
}

func BenchmarkJoinSize10(b *testing.B) {
	benchmarkJoinSize(b, 10)
}

func BenchmarkJoinSize100(b *testing.B) {
	benchmarkJoinSize(b, 100)
}

func BenchmarkJoinSize1000(b *testing.B) {
	benchmarkJoinSize(b, 1000)
}

func BenchmarkJoinSize10000(b *testing.B) {
	benchmarkJoinSize(b, 10000)
}

// benchmarkBufferString
func benchmarkBufferString(b *testing.B, numConcat int) {
	// Reports memory allocations
	b.ReportAllocs()

	var ns string
	for i := 0; i < b.N; i++ {
		next := nextString()
		buffer := bytes.NewBufferString("")
		for u := 0; u < numConcat; u++ {
			buffer.WriteString(next())
		}
		ns = buffer.String()
	}
	global = ns
}

func BenchmarkBufferString10(b *testing.B) {
	benchmarkBufferString(b, 10)
}

func BenchmarkBufferString100(b *testing.B) {
	benchmarkBufferString(b, 100)
}

func BenchmarkBufferString1000(b *testing.B) {
	benchmarkBufferString(b, 1000)
}

func BenchmarkBufferString10000(b *testing.B) {
	benchmarkBufferString(b, 10000)
}

// benchmarkBufferStringFunc
func benchmarkBufferStringFunc(str ...string) string {
	buffer := bytes.NewBufferString("")
	for _, s := range str {
		buffer.WriteString(s)
	}
	return buffer.String()
}

func BenchmarkBufferStringFunc10(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	str := make([]string, 10)
	next := nextString()
	for i := range str {
		str[i] = next()
	}
	b.StartTimer()
	var ns string
	for i := 0; i < b.N; i++ {
		ns = benchmarkBufferStringFunc(str...)
	}
	global = ns
}

func BenchmarkBufferStringFunc100(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	str := make([]string, 100)
	next := nextString()
	for i := range str {
		str[i] = next()
	}
	b.StartTimer()
	var ns string
	for i := 0; i < b.N; i++ {
		ns = benchmarkBufferStringFunc(str...)
	}
	global = ns
}

func BenchmarkBufferStringFunc1000(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	str := make([]string, 1000)
	next := nextString()
	for i := range str {
		str[i] = next()
	}
	b.StartTimer()
	var ns string
	for i := 0; i < b.N; i++ {
		ns = benchmarkBufferStringFunc(str...)
	}
	global = ns
}

func BenchmarkBufferStringFunc10000(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	str := make([]string, 10000)
	next := nextString()
	for i := range str {
		str[i] = next()
	}
	b.StartTimer()
	var ns string
	for i := 0; i < b.N; i++ {
		ns = benchmarkBufferStringFunc(str...)
	}
	global = ns
}

func benchmarkBufferSize(b *testing.B, numConcat int) {
	// Reports memory allocations
	b.ReportAllocs()

	var ns string
	for i := 0; i < b.N; i++ {
		next := nextString()
		buffer := bytes.NewBuffer(make([]byte, 0, numConcat*10))
		for u := 0; u < numConcat; u++ {
			buffer.WriteString(next())
		}
		ns = buffer.String()
	}
	global = ns
}

func BenchmarkBufferSize10(b *testing.B) {
	benchmarkBufferSize(b, 10)
}

func BenchmarkBufferSize100(b *testing.B) {
	benchmarkBufferSize(b, 100)
}

func BenchmarkBufferSize1000(b *testing.B) {
	benchmarkBufferSize(b, 1000)
}

func BenchmarkBufferSize10000(b *testing.B) {
	benchmarkBufferSize(b, 10000)
}
