package benchmark

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"encoding/xml"
	"testing"

	kelbin "github.com/kelindar/binary"
)

type ConvertMe struct {
	Name      string `protobuf:"bytes,1,opt,name=name,proto3" json:"name" xml:"name"`
	Age       int    `json:"age" xml:"age"`
	IsEnabled bool   `json:"is_enabled" xml:"is_enabled"`

	Address Address `json:"address" xml:"address"`
}

type Address struct {
	Street string `json:"street" xml:"street"`
	City   string `json:"city" xml:"city"`
	State  string `json:"state" xml:"state"`
	Zip    string `json:"zip" xml:"zip"`

	Coordinates Coordinates `json:"coordinates" xml:"coordinates"`
}

type Coordinates struct {
	Latitude  float64 `json:"latitude" xml:"latitude"`
	Longitude float64 `json:"longitude" xml:"longitude"`
}

var table = ConvertMe{
	Name:      "John Doe",
	Age:       32,
	IsEnabled: true,
	Address: Address{
		Street: "123 Main St",
		City:   "New York",
		State:  "NY",
		Zip:    "10001",
		Coordinates: Coordinates{
			Latitude:  40.7128,
			Longitude: -74.0059,
		},
	},
}

var tables []ConvertMe

func init() {
	for i := 0; i < 100; i++ {
		tables = append(tables, table)
	}
}

var xmlo, gobo, jsono, kelbino []byte

func BenchmarkKelbinMarshal(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		kelbino, _ = kelbin.Marshal(table)
	}
}

func BenchmarkKelbinUnMarchal(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		kelbin.Unmarshal(kelbino, &table)
	}
}

func BenchmarkXMLMarshal(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		xmlo, _ = xml.Marshal(table)
	}
}

func BenchmarkXMLUnMarshal(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		xml.Unmarshal(xmlo, &table)
	}
}

func BenchmarkGobEncode(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		gob.NewEncoder(&buf).Encode(table)
		gobo = buf.Bytes()
	}
}

func BenchmarkGobUnEncode(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		buf.Write(gobo)
		gob.NewDecoder(&buf).Decode(&table)
	}
}

func BenchmarkJsonMarshal(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		jsono, _ = json.Marshal(table)
	}
}

func BenchmarkJsonUnMarshal(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		json.Unmarshal(jsono, &table)
	}
}

// X100
func BenchmarkKelbinMarshalX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		kelbino, _ = kelbin.Marshal(tables)
	}
}

func BenchmarkKelbinUnMarchalX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		kelbin.Unmarshal(kelbino, &tables)
	}
}

func BenchmarkXMLMarshalX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		xmlo, _ = xml.Marshal(tables)
	}
}

func BenchmarkXMLUnMarshalX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		xml.Unmarshal(xmlo, &tables)
	}
}

func BenchmarkGobEncodeX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		gob.NewEncoder(&buf).Encode(tables)
		gobo = buf.Bytes()
	}
}

func BenchmarkGobUnEncodeX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		buf.Write(gobo)
		gob.NewDecoder(&buf).Decode(&tables)
	}
}

func BenchmarkJsonMarshalX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		jsono, _ = json.Marshal(tables)
	}
}

func BenchmarkJsonUnMarshalX100(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		json.Unmarshal(jsono, &tables)
	}
}
