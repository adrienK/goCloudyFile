// goos: linux
// goarch: amd64
// pkg: git.iglou.eu/Laboratory/goCloudyFile/benchmark
// cpu: Intel(R) Core(TM) i3-1005G1 CPU @ 1.20GHz
// BenchmarkHexStrFmt-4              	 2843338	       391.6 ns/op	     128 B/op	       3 allocs/op
// BenchmarkHexStrFmtSlice-4         	 6478960	       196.4 ns/op	      88 B/op	       2 allocs/op
// BenchmarkHexStrEncodeToString-4   	10390341	       114.9 ns/op	     128 B/op	       2 allocs/op
// BenchmarkHexStrURLEncode-4        	13156654	       109.0 ns/op	      96 B/op	       2 allocs/op
// BenchmarkInt64ToStrFmt-4          	 9842840	       116.6 ns/op	      32 B/op	       2 allocs/op
// BenchmarkInt64ToStrFint-4         	24115053	        49.60 ns/op	      24 B/op	       1 allocs/op
// BenchmarkInt64ToStrItoa-4         	25298514	        49.48 ns/op	      24 B/op	       1 allocs/op
// BenchmarkIntToStrFmt-4            	14647143	        86.48 ns/op	      16 B/op	       1 allocs/op
// BenchmarkIntToStrItoa-4           	45813770	        28.23 ns/op	       7 B/op	       0 allocs/op
// BenchmarkFloat64ToStrFmt-4        	 4437642	       272.2 ns/op	      16 B/op	       2 allocs/op
// BenchmarkFloat64ToStrFint-4       	 7177652	       157.5 ns/op	      48 B/op	       2 allocs/op
// BenchmarkFloat64ToStrFint2-4      	 5437197	       221.0 ns/op	      28 B/op	       2 allocs/op

package benchmark

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"math/rand"
	"strconv"
	"testing"
)

var glob string

var i = rand.Int()
var i64 = rand.Int63()
var f64 = rand.Float64()
var hexa = sha256.Sum256([]byte("The sleeper must awaken"))

func BenchmarkHexStrFmt(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = fmt.Sprintf("%x", hexa)
	}
	glob = buf
}

func BenchmarkHexStrFmtSlice(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = fmt.Sprintf("%x", hexa[:])
	}
	glob = buf
}

func BenchmarkHexStrEncodeToString(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = hex.EncodeToString(hexa[:])
	}
	glob = buf
}

// Or in base64 ...
func BenchmarkHexStrURLEncode(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = base64.URLEncoding.EncodeToString(hexa[:])
	}
	glob = buf
}

// BenchmarkInt64ToStrFmt is a benchmark for converting int64 to string
func BenchmarkInt64ToStrFmt(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = fmt.Sprintf("%d", i64)
	}
	glob = buf
}

func BenchmarkInt64ToStrFint(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = strconv.FormatInt(i64, 10)
	}
	glob = buf
}

// Lost some data ...
func BenchmarkInt64ToStrItoa(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = strconv.Itoa(int(i64))
	}
	glob = buf
}

// BenchmarkIntToStrFmt is a benchmark for converting int to string
func BenchmarkIntToStrFmt(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = fmt.Sprintf("%d", i)
	}
	glob = buf
}

func BenchmarkIntToStrItoa(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = strconv.Itoa(i)
	}
	glob = buf
}

// BenchmarkFloat64ToStrFmt is a benchmark for converting float64 to string
func BenchmarkFloat64ToStrFmt(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = fmt.Sprintf("%f", f64)
	}
	glob = buf
}

func BenchmarkFloat64ToStrFint(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = strconv.FormatFloat(f64, 'f', -1, 64)
	}
	glob = buf
}

// Lost some data ...
func BenchmarkFloat64ToStrFint2(b *testing.B) {
	b.ReportAllocs()

	var buf string
	for i := 0; i < b.N; i++ {
		buf = strconv.FormatFloat(f64, 'f', 2, 64)
	}
	glob = buf
}
