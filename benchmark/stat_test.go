package benchmark

import (
	"io/fs"
	"os"
	"testing"
)

var fi fs.FileInfo

// BenchmarkStat benchmark os.Stat function
func BenchmarkStat(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		fi, _ = os.Stat(".")
	}
}

func BenchmarkOpenStat(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		f, _ := os.Open(".")
		fi, _ = f.Stat()
		f.Close()
	}
}
